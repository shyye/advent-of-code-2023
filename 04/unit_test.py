# Chat GPT help
import unittest
from part1 import calculate_points


class TestCalculatePoints(unittest.TestCase):
    def test_calculate_points(self):
        # Test with 0 winning cards
        self.assertIs(calculate_points(0), 0)

        # Test with 1 winning card
        self.assertEqual(calculate_points(1), 1)

        # Add more test cases as needed
        self.assertEqual(calculate_points(4), 8)


if __name__ == '__main__':
    unittest.main(verbosity=2)
