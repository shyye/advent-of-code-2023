

class Main:
    #
    # Set day: ----------------------------
    #
    DAY = "07"
    #
    # Set day ^ ---------------------------

    def __init__(self, day=DAY):
        self.__day = str(day)
        self.__filepath = str(day) + "\\input.txt"
        self.__result = None

    # Getters
    def get_day(self):
        return self.__day

    def get_filepath(self):
        return self.__filepath

    def get_result(self):
        return self.__result

    # Setters
    def set_result(self, result):
        self.__result = result

    # Maon
    def main(self):
        print(
            """
             ________________
            |                |
            |   MAIN CLASS   |
            |________________|
            """
            )

        self.print_result_part1()
        self.print_result_part2()

    # Public methods
    def print_result_part1(self):
        result = self.get_result()
        day = self.get_day()

        print("\nAdvent of Code 2023")
        print(f"Day {day}\n")

        if result is None:
            self.__error_msg_result_not_defined()
        else:
            print("\tResult (part 1):", result)
            print()

    def print_result_part2(self):
        result = self.get_result()
        day = self.get_day()

        print("\nAdvent of Code 2023")
        print(f"Day {day}\n")

        if result is None:
            self.__error_msg_result_not_defined()
        else:
            print("\tResult (part 2):", result)
            print()

    # Private
    def __error_msg_result_not_defined(self):
        print("\033[91m\tResult is not defined\033[0m")
        print("\033[93m\tPlease use set_result() to define it\033[0m")
        print()


if __name__ == "__main__":

    # Set day:
    #
    DAY = "00"

    Main(DAY).main()
