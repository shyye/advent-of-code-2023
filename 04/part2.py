# Reference / links
# https://youtu.be/ivl5-snqul8?si=PlbponlC9GAzYtid

from main import Main
import numpy as np

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()


# Methods
def calculate_points(num_of_winning_numbers):
    cards = int(num_of_winning_numbers)

    if cards == 0:
        return 0

    points = 1
    for _ in range(cards - 1):
        points = points * 2

    return points


def filter_format_array(values_string):    
    format_array = values_string.strip().split(" ")    
    numpy_array = np.array(format_array)                
    filtered_array = numpy_array[numpy_array != '']   
    int_array = filtered_array.astype(int)

    return filtered_array, int_array


# FIRST TRY
def copy_card(cards, index, points):
    # for p in points:
    #     get next card
    #     copy card 
    #     count card
    #         for p in points in copied card
    #             get next card
    #             copy card
    #             coint card
    #               ......return card count

    # for card in cards:
    #      count card + 1
    #      get card points
    #      for p in points:
    #           copy card p1
    #             for p points in card p1
    #                 copy card pp1
    #                     for p points in pp2
    #                         copy card ppp2
    #                             if points = 0
    #                                 return 1
    #                             else
    #                                return points += copy_card(cards, index - 1, points - 2)
    #                 copy card pp2
    #           copy card p2
    #           copy card p3
    points = cards[index]

    if points == 0:
        return 1
    else:
        return 1 + copy_card(cards, index - 1, points - 1)


# SECOND TRY
def copy_cards(cards, index, card_count):
    if index >= len(cards) - 1:
        return int(card_count)

    points = cards[index]

    copied_cards = []

    copy_index = index
    while True:
        if points == 0:
            break
        if copy_index == len(cards) - 1:
            card_count += 1
            break
        if copy_index > len(cards) - 1:
            break
        
        copy_index += 1
        copied_cards.append(cards[copy_index])
        card_count += 1
        points -= 1

    for index, copy in enumerate(copied_cards):
        copy_cards(copied_cards, index, card_count)

    return int(card_count)


# ChatGPT help with input from own code
def count_copies(cards, index):
    points = cards[index]

    if points == 0:
        return 1

    total_copies = 1  # Count the current card

    for i in range(index + 1, min(index + points + 1, len(cards))):
        # Recursively count copied cards
        total_copies += count_copies(cards, i)

    return total_copies


# Main
with open(FILE_PATH) as file:
    content = file.readlines()

    total_points = 0
    cards = []

    for line in content:
        # Data
        card, data = line.split(":")
        winning_values, user_values = data.split("|")

        # Format and get numpy arrays with int values
        winning_values = filter_format_array(winning_values)[1] 
        user_values = filter_format_array(user_values)[1]

        check_is_in = np.isin(user_values, winning_values)

        count_true = np.count_nonzero(check_is_in)

        points = count_true

        # Save card
        cards.append(points)


    # NOT WORKING
    card_count = 0
    for index, card in enumerate(cards):
        # card_count += copy_cards(cards, index, 0)
        card_count += copy_cards(cards, index, 0)
        print(card_count)

    # card_count = copy_card(cards, index, points)
    print(cards)


    # ChatGPT
    total_copies = 0
    for index, card in enumerate(cards):
            # Count copies for each card
        total_copies += count_copies(cards, index)

    print(total_copies)


result = total_copies


# Print Result
main.set_result(result)
main.print_result_part2()


# NOTES
# for p in points:
#     get next card
#     copy card
#     count card
#         for p in points in copied card
#             get next card
#             copy card
#             coint card
#               ......return card count

# for card in cards:
#      get card points
#      for p in points:
#           copy card p1
#             for p points in card p1
#                 copy card pp1
#                     for p points in pp2
#                         copy card ppp2
#                             if points = 0
#                                 return 1
#                             else
#                                return count_card += copy card
#                 copy card pp2
#           copy card p2
#           copy card p3
