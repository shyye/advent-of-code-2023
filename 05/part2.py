from main import Main
import numpy as np

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()


# Methods
def convert_map(map, elements_to_map):

    corresponding_elements = []

    for row in map:
        destination_start = int(row[0])
        source_start = int(row[1])
        length = int(row[2])

        destination_end = destination_start + length
        source_end = source_start + length

        for element in elements_to_map:
            element = int(element)

            if element >= source_start and element <= source_end:
                element_range_from_start = element - source_start
                corresponding_element = destination_start + element_range_from_start
                corresponding_elements.append(corresponding_element)
            else:
                corresponding_element = element

    return corresponding_elements


def get_sorted_source_range(destination_range, next_map):

    sorted_corresponding_range = []

    for d_range in destination_range:

        source_start = d_range[0]     # E.g. Humidity
        source_end = d_range[1]       # e.g. Humidity
        destination_start = d_range[2]        # e.g. Location
        destination_end = d_range[3]          # E.g. Location


        for range in next_map:

            new_source_start = range[0]                 # E.g. Temperature
            new_source_end = range[1]                   # E.g. Temperature
            new_destination_start = range[2]      # E.g. Humidity 
            new_destination_end = range[3]         # E.g. HUmidity 

            if source_start == new_destination_start and source_end == new_destination_end:
                sorted_corresponding_range.append(range)
                print(range)
                break

            # # How many numbers are in the map range
            # last_element = None
            # if new_source_start <= new_destination_end:
        
            #     if new_source_end > new_destination_end:
            #         last_element = new_destination_end
            #     else:
            #         last_element = new_source_end

            #     numbers_from_end = None
            #     numbers_from_start = None

            #     if last_element < new_source_end:           #TODO: kanske blir fel här
            #         numbers_from_end = new_source_end - last_element

            #     first_element = None
            #     if new_destination_start < new_source_start:
            #         first_element = new_source_start
            #     else:
            #         first_element = new_destination_start


            #     if first_element == new_source_start:
            #         numbers_from_start = 0
            #     else:
            #         numbers_from_start = new_source_end - 

    return sorted_corresponding_range





def find_ranges(map):

    ranges_arr = []

    for row in map:
        destination_start = int(row[0])
        source_start = int(row[1])
        length = int(row[2])

        destination_end = destination_start + length
        source_end = source_start + length

        map_range = [source_start, source_end, destination_start, destination_end]
        ranges_arr.append(map_range)

    return ranges_arr



# Code
with open(FILE_PATH) as file:
    # Store seeds and maps
    seeds = file.readline().strip()
    seed_to_soil_map = []
    soil_to_fertilizer_map = []
    fertilizer_to_water_map = []
    water_to_light_map = []
    light_to_temperature_map = []
    temperature_to_humidity_map = []
    humidity_to_location_map = []

    content = file.readlines()
    almanac_map = []
    for line in content:
        line = line.strip()

        # Set correct map to store values in
        match line:
            case 'seed-to-soil map:':
                almanac_map = seed_to_soil_map
            case 'soil-to-fertilizer map:':
                almanac_map = soil_to_fertilizer_map
            case 'fertilizer-to-water map:':
                almanac_map = fertilizer_to_water_map
            case 'water-to-light map:':
                almanac_map = water_to_light_map
            case 'light-to-temperature map:':
                almanac_map = light_to_temperature_map
            case 'temperature-to-humidity map:':
                almanac_map = temperature_to_humidity_map
            case 'humidity-to-location map:':
                almanac_map = humidity_to_location_map
            case '':
                almanac_map = []

        almanac_values = line.split(" ")        
        almanac_map.append(almanac_values)

    # Get seed ranges that corresponds to lowest destination ranges
    # Search lowest Destination ranges for location / lowest locations

    ranges_seed_to_soil = find_ranges(seed_to_soil_map[1:])
    ranges_soil_to_fertilizer = find_ranges(soil_to_fertilizer_map[1:])
    ranges_fertilizer_to_water = find_ranges(fertilizer_to_water_map[1:])
    ranges_water_to_light = find_ranges(water_to_light_map[1:])
    ranges_light_to_temperature = find_ranges(light_to_temperature_map[1:])
    ranges_temperature_to_humidity = find_ranges(temperature_to_humidity_map[1:])
    ranges_humidity_to_location = find_ranges(humidity_to_location_map[1:])

    # print(ranges_temperature_to_humidity)

    for i in ranges_temperature_to_humidity:
        print(i)

    # Sort by Destination Start
    sorted_by_location_ranges = sorted(ranges_humidity_to_location, key=lambda x: x[2])

    temp_from_hum = get_sorted_source_range(sorted_by_location_ranges, ranges_temperature_to_humidity)


    test_map = [[1, 4, 2],[1, 2, 3]]
    test_range = find_ranges(test_map)
    print(test_range)

    test_range_destination = [[1, 5, 4, 6], [1,4, 2, 3]]
    test_range_next_map = [[10, 12, 1, 5], [20 , 22, 1, 4], [9, 10, 10, 15]]

    test_convert = get_sorted_source_range(test_range_destination, test_range_next_map)

    print("Test convert")
    print(test_convert)




    print(temp_from_hum)

    # # Map lowest location range to seed ranges
    # for lowest_location_range in sorted_by_location_ranges:

    #     source_start = lowest_location_range[0]     # Humidity
    #     source_end = lowest_location_range[1]       # Humidity
    #     destination_start = lowest_location_range[2]        # Location
    #     destination_end = lowest_location_range[3]          # Location

    #     # In next map Humidity is Destination and the source is temprature
    #     for range in ranges_temperature_to_humidity:

    #         source_start = range[0]         # temperature
    #         source_end = range[1]           # temperature
    #         destination_start = range[2]    # humidity
    #         destination_end = range[3]      # humidity

    #         if seed_start <= source_end:
    #             # How many seed numbers are in the map range
    #             last_seed = None
    #             if seed_end > source_end:
    #                 last_seed = source_end
    #             else:
    #                 last_seed = seed_end

    #             numbers_from_end = None
    #             numbers_from_start = None

    #             if last_seed < source_end:
    #                 numbers_from_end = source_end - last_seed

    #             first_seed = None
    #             if seed_start < source_start:
    #                 first_seed = source_start
    #             else:
    #                 first_seed = seed_start


    #             if first_seed != source_start:

result = None


# Print Result
main.set_result(result)
main.print_result_part2()
