import math

# Constants
FILE_PATH = "01\\input.txt"
DIGITS_IN_WORDS = {
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9
}


def find_first_digit(line):
    for char in line:
        if char.isdigit():
            index = line.find(char)
            return (char, index)


def find_last_digit(line):
    reversed_line = line[::-1]
    for char in reversed_line:
        if char.isdigit():
            index = line.rfind(char)
            return (char, index)
        

def find_first_digit_word(line):
    first = (-1, math.inf)

    for word, value in DIGITS_IN_WORDS.items():
        valid_word_index = line.find(word)

        if valid_word_index < first[1] and valid_word_index >= 0:
            first = (value, valid_word_index, word)

    return first


def find_last_digit_word(line):
    last = (-1, -1)

    for word, value in DIGITS_IN_WORDS.items():
        valid_word_index = line.rfind(word)

        if valid_word_index > last[1]:
            last = (value, valid_word_index, word)

    return last


# Store sum
sum = 0

# Get file content - NOT GOOD PRACTICE, NEVER CLOSES FILE
# file = open(FILE_PATH, "r").readlines()
file_ = open(FILE_PATH, "r")
file = file_.readlines()

for line in file:
    # Find first digits
    first_digit = find_first_digit(line)
    last_digit = find_last_digit(line)

    # Find digit words (digit letters)
    first_digit_word = find_first_digit_word(line)
    last_digit_word = find_last_digit_word(line)

    # Compare index of first digit
    if first_digit[1] > first_digit_word[1]:
        first = first_digit_word[0]
    else:
        first = first_digit[0]

    # Compare index of last digit
    if last_digit[1] > last_digit_word[1]:
        last = last_digit[0]
    else:
        last = last_digit_word[0]

    # Add to sum
    digit = str(first) + str(last)
    sum += int(digit)

    print("Line:", line, end="")
    print("Digit", first_digit, last_digit)
    print("Word:", first_digit_word, last_digit_word)
    print("First:", first)
    print("Last:", last)
    print("First + last:", digit)
    print("sum:", sum)
    print()

file_.close()

# Print result
print("\nAdvent of Code 2023 - Day 1, Part 2\n")
print("Result:", sum)
print()
