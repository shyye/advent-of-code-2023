import numpy as np

FILE_PATH = "02\\input.txt"
result = None

def get_possible_games(red, green, blue, text_input):

    # Store game ID count
    sum = 0
    possible_games = []

    for line in text_input:

        # TODO: Hur mycket tar split funktionen? Onödigt dåligt att göra så många separata steg???
        game, data = line.split(":")

        game_number = game.split(" ")[1]
        sets = data.split(";")

        print("\nGame:", game_number)
        # print("Data:", data)
        # print("===>:", sets)

        numpy_outer_array = np.empty((0, 3), int)
        for set in sets:
            # Numpy, inner array for organizing colors
            # [0] = red
            # [1] = green
            # [2] = blue
            numpy_inner_rgb_array = np.array([0, 0, 0])
            
            items = set.split(",")
            for item in items:
                amount, item_color = item.strip().split(" ")
                
                # Convert
                amount = int(amount)
                
                # Place color count at their specific position
                match item_color:
                    case "red":
                        numpy_inner_rgb_array[0] = amount
                    case "green":
                        numpy_inner_rgb_array[1] = amount
                    case "blue":
                        numpy_inner_rgb_array[2] = amount
      
            # Vertical stack     
            numpy_outer_array = np.vstack((numpy_outer_array, numpy_inner_rgb_array))

        print("Numpy outer array:")    
        print(numpy_outer_array)

        # Check condition
        r = np.all(numpy_outer_array[:, 0] <= red)
        g = np.all(numpy_outer_array[:, 1] <= green)
        b = np.all(numpy_outer_array[:, 2] <= blue)

        if r and g and b:
            # print("<-----------------------------------------Game " + game_number + " is OK!")

            sum += int(game_number)
            possible_games.append("Game: " + game_number)

    return sum, possible_games


with open(FILE_PATH) as file:
    # Store file content
    content = file.readlines()

    # Cubes
    red = 12
    green = 13
    blue = 14

    result, possible_games = get_possible_games(red, green, blue, content)

# Print result
print("\nAdvent of Code 2023")
print("Day 2, Part 1\n")
for i in possible_games:
    print(i)
print()
print("\tResult:", result)
print()





'''
Sketch:

game    set1    set2    set3    set4(?)    possiblie        - could be 3 or 4 sets?
1       g,r,b   r,b,g   ...     ....       yes/no           - different order


Sort columns in all sets as
red, green, blue

_extract
(amount, color)
_if color is red
put in position 0
_if color is green
put in position 1 etc...
_If value for color X doesn't exist
value = 0


Numpy sum columns 2d array
[
    [r, g, b]               - set1
    [r, g, b]               - set2
    [r, g, b]               - set3
]

Output sum of:
[r, g, b]

Convert to True / False

Store the ID's if True

Sum the ID's

'''
