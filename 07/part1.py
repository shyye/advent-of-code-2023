#
#
# NOT FINISHED
#
#

from main import Main
import numpy as np

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()

# Store cards
five_of_a_kind = []
four_of_a_kind = []
three_of_a_kind = []
full_house = []
two_pair = []
one_pair = []
high_card = []


# Methods
def check_type(hand):
    cards = [*hand[0]]      # Unpack string into list elements
    bid = hand[1]

    # Cards and bid
    card_bid = [*hand[0]] 
    card_bid.append(bid)

    cards_np = np.array(cards)

    # print(cards)
    # print(cards_np)
    is_five_of_a_kind = np.all(cards_np == cards_np[0])
    # is_five_of_a_kind = np.all(cards)
    # print(is_five_of_a_kind)

    # Get number of occurences for unique counts
    count_unique = np.unique(cards_np, return_counts=True)[1]

    is_four_of_a_kind = (count_unique[0] == 1 and count_unique[1] == 4) or (count_unique[0] == 4 and count_unique[1] == 1)

    is_full_house = (count_unique[0] == 2 and count_unique[1] == 3) or (count_unique[0] == 3 and count_unique[1] == 2)

    # Not very nice duplicate code, #TODO: Fix
    count_unique = np.unique(cards_np, return_counts=True)
    num_of_unique = len(count_unique[0])
    is_three_of_a_kind = (num_of_unique) == 3 and (np.max(count_unique[1]) != 2)

    # Must be a better way #TODO: Fix methods instead
    count_unique = count_unique[1]      # occurences
    print(count_unique)
    is_two_pair = (num_of_unique == 3) and (np.max(count_unique) == 2)

    is_one_pair = (num_of_unique == 4)
    
    is_high_card = (num_of_unique == 5)


    print([hand[0]])

    if is_five_of_a_kind:
        # print(is_five_of_a_kind, "- Five of a Kind")
        five_of_a_kind.append(card_bid)

    if is_four_of_a_kind:
        # print(is_four_of_a_kind, "- Four of a Kind")
        four_of_a_kind.append(card_bid)

    if is_full_house:
        # print(is_full_house, "- Full house")
        full_house.append(card_bid)

    if is_three_of_a_kind:
        # print(is_three_of_a_kind, "- Three of a kind")
        three_of_a_kind.append(card_bid)

    if is_two_pair:
        # print(is_two_pair, "- Two pair")
        two_pair.append(card_bid)

    if is_one_pair:
        # print(is_one_pair, "- One pair")
        one_pair.append(card_bid)

    if is_high_card:
        # print(is_high_card, "- High Card")
        high_card.append(card_bid)

    print()
    


    # hand = [1, 2, 3, 4, 5, bid]
    # high_card.append(card_bid)

    

# Main
with open(FILE_PATH) as file:
    content = file.readlines()


# content = ['AAAAA 10\n', 'AA8AA 10\n', '23332 10\n', 'TTT98 10\n', '23432 10\n', 'A23A4 10\n', '23456 10\n']

content = content[:10]

result = 0
for line in content:
    # print(line)
    
    hand = line.strip().split()


    # Check and Put in suitable array
    hand_type = check_type(hand)       

    different_hand_types = [high_card, one_pair, two_pair, full_house,
                            three_of_a_kind, four_of_a_kind, five_of_a_kind]
    
    for h_type in different_hand_types:

        # Sort
        h_type_np = np.vstack(h_type)
        # card_1, card_2, card_3, card_4, card_5 = h_type_np[0], h_type_np[1], h_type_np[2], h_type_np[3], h_type_np[4]

        print(h_type)
        # sorted_hand_type = sorted_hand_type

        
        # print(sorted_hand_type)

        rank = 1


result = None


# Print Result
main.set_result(result)
main.print_result_part1()

print("Note: Using Numpy, coul have practiced and make own functions")
