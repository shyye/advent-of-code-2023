from main import Main

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()


# Code
with open(FILE_PATH) as file:
    row_1 = file.readline().split(":")[1].strip()
    row_2 = file.readline().split(":")[1].strip()

times = list(map(int, row_1.split()))
record_distances = list(map(int, row_2.split()))

result = 1

# race = (time, record_distance)
boat_races = list(zip(times, record_distances))
# boat_races = [(7,9)]
for race in boat_races:
    num_of_ways_to_beat_record = 0

    time_limit = race[0]
    record = race[1]

    speed = 0

    for time in range(1, time_limit-1):
        speed = time
        time_to_move = time_limit - time
        distance = time_to_move * speed

        if distance > record:
            num_of_ways_to_beat_record += 1

    result *= num_of_ways_to_beat_record


# Print Result
main.set_result(result)
main.print_result_part1()
