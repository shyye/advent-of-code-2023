import re

FILE_PATH = "03\\input.txt"
result = None

with open(FILE_PATH) as file:

    # Store sum
    sum = 0

    # Store file content
    content = file.readlines()
    lines = []

    for line in content:
        lines.append(line)

    for line in lines:
        print(line)

        # Line setup
        line_index = lines.index(line) 
        start_position_line = 0         # Start position of string on a line
        last_line_index = (len(lines) - 1)

        graphics = []

        while True:
            graphic_item = []

            if len(line) < 1:
                break

            # Start position 
            top_line = lines[(line_index - 1)][start_position_line:]
            line = lines[(line_index)][start_position_line:]
            # TODO: imporve structure
            try:
                bottom_line = lines[(line_index + 1)][start_position_line:]
            except IndexError:
                bottom_line = None

            # Find next digit
            try:
                match_digit = re.search(r'\d+', line)
                digit = match_digit.group()
                digit_start_index = match_digit.start()
                digit_end_index = match_digit.end()
            except AttributeError:
                break

            # Initialize left, right
            left = None
            right = None

            # If first index
            if digit_start_index == 0:
                left = digit_start_index
            else:
                left = digit_start_index - 1

            right = digit_end_index + 1

            # Top search
            if line_index != 0:
                # If last index
                if digit_end_index == len(top_line) - 1:
                    right -= 1

                top_search = top_line[left:right]
                graphic_item.append(top_search)

            # This line search
            if digit_end_index == len(line) - 1:
                right -= 1

            side_search = line[left:right]       
            graphic_item.append(side_search)

            # Bottom search
            if line_index < last_line_index:
                #If last index
                if digit_end_index == len(bottom_line) - 1:
                    right -= 1

                bottom_search = bottom_line[left:right]
                # print(f"\t\t{bottom_search}")
                graphic_item.append(bottom_search)

            # New start position
            start_position_line += digit_end_index

            graphics.append(graphic_item)

            # Check if valid digit
            item_string = ""
            for line in graphic_item:
                item_string += line

            no_dots_no_digits = r'[^.\d]'
            valid = re.search(no_dots_no_digits, item_string)

            if valid:
                # print(item_string, "\t <----------- OK!")
                sum += int(digit)
            else:
                # print()
                # print(item_string, "\t <----------- Not valid!")
                pass
    

        # Chat GPT for graphic shit
        transposed_array = list(map(list, zip(*graphics)))

        for elements_at_position in transposed_array:
            for element in elements_at_position:
                print(f"\t{element}\t", end="")
            print()

        print("\n\n\n")

result = sum

# Print result
print("\nAdvent of Code 2023")
print("Day 3\n")
print("\tResult:", result)
print()
