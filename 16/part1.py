from main import Main
import numpy as np

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()

# Grid
grid = []
with open(FILE_PATH) as file:
    content = file.readlines()

    for line in content:
        line = line.strip()
        grid.append(list(line))

grid = np.array(grid)
beam_grid = np.array(grid)      # For visualizaition


# Over the edge cases
row_length, column_length = grid.shape
top_edge = 0                    # first row
bottom_edge = column_length-1     # last row
left_edge = 0                   # first column
right_edge = row_length-1         # last column


# Class
class Beam:

    def __init__(self, start_x, start_y, direction):
        self.x = start_x
        self.y = start_y
        self.direction = direction
        beam_grid[self.y, self.x] = '#'

    def move(self, direction):
        self.direction = direction

        beam_grid[self.y, self.x] = '#'

        print("Grid 1")
        print("Beam direction:", self.direction)
        print("X:", self.x, "Y:", self.y)
        print(beam_grid)
                
        match self.direction:
            case 'left':
                if self.x == left_edge:
                    return False
                self.x -= 1

            case 'right':
                if self.x == right_edge:
                    return False
                self.x += 1

            case 'up':
                if self.y == top_edge:
                    return False
                self.y -= 1

            case 'down':
                if self.y == bottom_edge:
                    return False
                self.y += 1

        beam_grid[self.y, self.x] = '#'
        print("\nGrid 2")
        print("Beam direction:", self.direction)
        print("X:", self.x, "Y:", self.y)
        # print(beam_grid)

        return True


    def get_direction(self):
        return self.direction
    
    def get_positions(self):
        return self.x, self.y
    
    def set_direction(self, direction):
        self.direction = direction


# Main
first_beam = Beam(0, 0, 'right')
# beam_grid[0, 0] = '#'

active_beams = [first_beam]
# done = len(active_beams) == 0

while not (len(active_beams) == 0):

    is_possible_to_move = True
    for beam in active_beams: 

        while is_possible_to_move:

            direction = beam.get_direction()
            column, row = beam.get_positions()
            character = grid[row, column]

            # graphic
            # beam_grid[row, column] = '#'

            match (direction + "+" + character):
                # Mirrors
                case 'up+/':
                    # direction = 'right'
                    beam.set_direction('right')
                
                case 'up+\\':
                    # direction = 'left'
                    beam.set_direction('left')

                case 'down+/':
                    # direction = 'left'
                    beam.set_direction('left')

                case 'down+\\':
                    # direction = 'right'
                    beam.set_direction('right')

                case 'left+/':
                    # direction = 'down'
                    beam.set_direction('down')

                case 'left+\\':
                    # direction = 'up'
                    beam.set_direction('up')

                case 'right+/':
                    # direction = 'up'
                    beam.set_direction('up')

                case 'right+\\':
                    # direction = 'down'
                    beam.set_direction('down')

                # Splitters
                case 'up+-':
                    beam.set_direction('left')
                    new_beam = Beam(column, row, 'right')
                    active_beams.append(new_beam)
                
                case 'down+-':
                    # TODO: DUPLICATED CODE FIX
                    beam.set_direction('left')
                    new_beam = Beam(column, row, 'right')
                    active_beams.append(new_beam)

                    # TODO: Borde va nån annanstans?
                    # Fixar att det räknas som en BEam även om det är sista innan den går över
                    # ett edge case
                    # beam_grid[row, column-1] = '#'
                    # beam_grid[row, column+1] = '#'

                case 'left+|':
                    beam.set_direction('up')
                    new_beam = Beam(column, row, 'down')
                    active_beams.append(new_beam)

                    # TODO: Borde va nån annanstans?
                    # Fixar att det räknas som en BEam även om det är sista innan den går över
                    # # ett edge case
                    # beam_grid[row-1, column] = '#'
                    # beam_grid[row+1, column] = '#'

                case 'right+|':
                    # TODO: DUPLICATED CODE FIX
                    beam.set_direction('up')
                    new_beam = Beam(column, row, 'down')
                    active_beams.append(new_beam)

                    # TODO: Borde va nån annanstans?
                    # Fixar att det räknas som en BEam även om det är sista innan den går över
                    # ett edge case
                    # beam_grid[row-1, column] = '#'
                    # beam_grid[row+1, column] = '#'

            direction = beam.get_direction()
            is_possible_to_move = beam.move(direction)       

        active_beams.remove(beam)  

    # done = len(active_beams) == 0



print(grid)
print()
print(beam_grid)

beams_count = np.count_nonzero(beam_grid == '#')

result = beams_count


# Print Result
main.set_result(result)
main.print_result_part1()
