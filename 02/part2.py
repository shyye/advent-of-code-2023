import numpy as np

FILE_PATH = "02\\input.txt"
result = None

def get_possible_games(text_input):

    sum = 0

    for line in text_input:

        # TODO: Hur mycket tar split funktionen? Onödigt dåligt att göra så många separata steg???
        game, data = line.split(":")

        game_number = game.split(" ")[1]
        # print("\nGame:", game_number)
        sets = data.split(";")

        numpy_outer_array = np.empty((0, 3), int)
        for set in sets:
            # Numpy, inner array for organizing colors
            # [0] = red
            # [1] = green
            # [2] = blue
            numpy_inner_rgb_array = np.array([0, 0, 0])
            
            items = set.split(",")
            for item in items:
                amount, item_color = item.strip().split(" ")
                
                # Convert
                amount = int(amount)
                
                # Place color count at their specific position
                match item_color:
                    case "red":
                        numpy_inner_rgb_array[0] = amount
                    case "green":
                        numpy_inner_rgb_array[1] = amount
                    case "blue":
                        numpy_inner_rgb_array[2] = amount
      
            # Vertical stack     
            numpy_outer_array = np.vstack((numpy_outer_array, numpy_inner_rgb_array))

        # Get the valuue of the highest count for r, g, b respectively
        r = np.max(numpy_outer_array[:, 0])
        g = np.max(numpy_outer_array[:, 1])
        b = np.max(numpy_outer_array[:, 2])

        # print(str(r), str(g), str(b), sep=" + ")

        cubes = r*g*b
        sum += cubes

    return sum


with open(FILE_PATH) as file:
    # Store file content
    content = file.readlines()
    result = get_possible_games(content)

# Print result
print("\nAdvent of Code 2023")
print("Day 2, Part 1\n")
print("\tResult:", result)
print()



