# References
# https://www.tutorialspoint.com/how-to-check-whether-specified-values-are-present-in-numpy-array
# https://www.statology.org/numpy-count-true/

from main import Main
import numpy as np

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()


# Methods
def calculate_points(num_of_winning_numbers):
    cards = int(num_of_winning_numbers)

    if cards == 0:
        return 0

    points = 1
    for _ in range(cards - 1):
        points = points * 2

    return points


def filter_format_array(values_string):    
    format_array = values_string.strip().split(" ")    
    numpy_array = np.array(format_array)                
    filtered_array = numpy_array[numpy_array != '']   
    int_array = filtered_array.astype(int)

    return filtered_array, int_array


# Main
with open(FILE_PATH) as file:
    content = file.readlines()

    total_points = 0

    for line in content:
        # Data
        card, data = line.split(":")
        winning_values, user_values = data.split("|")

        # Format and get numpy arrays with int values
        winning_values = filter_format_array(winning_values)[1] 
        user_values = filter_format_array(user_values)[1]

        check_is_in = np.isin(user_values, winning_values)

        count_true = np.count_nonzero(check_is_in)

        points = calculate_points(count_true)
 
        # Add result to points
        total_points += points

        # print(check_is_in)
        # print(count_true)


result = total_points


# Print Result
main.set_result(result)
main.print_result_part1()
