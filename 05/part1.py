from main import Main
import numpy as np

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()


# Methods
def convert_map(map, elements_to_map):

    corresponding_elements = []

    for row in map:
        destination_start = int(row[0])
        source_start = int(row[1])
        length = int(row[2])

        destination_end = destination_start + length
        source_end = source_start + length

        for element in elements_to_map:
            element = int(element)

            if element >= source_start and element <= source_end:
                element_range_from_start = element - source_start
                corresponding_element = destination_start + element_range_from_start
                corresponding_elements.append(corresponding_element)
            else:
                corresponding_element = element

    return corresponding_elements



# Code
with open(FILE_PATH) as file:
    # Store seeds and maps
    seeds = file.readline().strip()
    seed_to_soil_map = []
    soil_to_fertilizer_map = []
    fertilizer_to_water_map = []
    water_to_light_map = []
    light_to_temperature_map = []
    temperature_to_humidity_map = []
    humidity_to_location_map = []

    content = file.readlines()
    almanac_map = []
    for line in content:
        line = line.strip()

        # Set correct map to store values in
        match line:
            case 'seed-to-soil map:':
                almanac_map = seed_to_soil_map
            case 'soil-to-fertilizer map:':
                almanac_map = soil_to_fertilizer_map
            case 'fertilizer-to-water map:':
                almanac_map = fertilizer_to_water_map
            case 'water-to-light map:':
                almanac_map = water_to_light_map
            case 'light-to-temperature map:':
                almanac_map = light_to_temperature_map
            case 'temperature-to-humidity map:':
                almanac_map = temperature_to_humidity_map
            case 'humidity-to-location map:':
                almanac_map = humidity_to_location_map
            case '':
                almanac_map = []

        almanac_values = line.split(" ")        
        almanac_map.append(almanac_values)

    # print(seed_to_soil_map, soil_to_fertilizer_map, fertilizer_to_water_map,
    #       water_to_light_map, light_to_temperature_map,
    #       temperature_to_humidity_map, humidity_to_location_map,
    #       sep="\n\n")
    
    # for seed in seeds:
    # for row in seed_to_soil_map[1:]:

    #     destination_start = int(row[0])
    #     source_start = int(row[1])
    #     length = int(row[2])

        # column_1 = np.arange(destination_start, (destination_start + length))
        # column_2 = np.arange(source_start, (source_start + length))

        # transformed_map = np.vstack((column_1, column_2)).T
        # print(transformed_map.shape)

        # if seed_number 

    test_map = [[2, 4, 3],[50, 98, 2],[52, 50, 48]]

    test_seed = [5, 98, 99, 52, 0]


    test = convert_map(test_map, test_seed)
    print(test)

    print(seeds)
    seeds = seeds.split(" ")

    soil = convert_map(seed_to_soil_map[1:], seeds[1:])
    fertilizer = convert_map(soil_to_fertilizer_map[1:], soil)
    water = convert_map(fertilizer_to_water_map[1:], fertilizer)
    light = convert_map(water_to_light_map[1:], water)
    temperature = convert_map(light_to_temperature_map[1:], light)
    humidity = convert_map(temperature_to_humidity_map[1:], temperature)
    location = convert_map(humidity_to_location_map[1:], humidity)


    print(water_to_light_map)
    print("MAP:")
    print(light)

    print(min(location))


result = None


# Print Result
main.set_result(result)
main.print_result_part1()
