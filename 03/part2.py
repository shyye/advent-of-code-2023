import re

FILE_PATH = "03\\input.txt"
result = None


def left_right_digit_search(left, right, x_search, x_line):
    # left
    while True:
        if left == 0:        # Nooo fix, not niice
            x_search = x_line[left:right]
            break

        is_left_digit = str(x_search[0]).isdigit()
        if is_left_digit:
            left -= 1
        else:
            # left += 1
            # x_search = x_line[left:right]
            break

        x_search = x_line[left:right]

    # right
    while True:
        if right == len(x_line) - 1:        # Nooo fix, not niice
            x_search = x_line[left:right]
            break

        is_right_digit = str(x_search[-1]).isdigit()
        if is_right_digit:
            right += 1
        else:
            break

        x_search = x_line[left:right]

    # result
    return x_search


# Main
with open(FILE_PATH) as file:

    # Store sum
    sum = 0

    # Store file content
    content = file.readlines()
    lines = []

    for line in content:
        lines.append(line)

    # Handle each line/row
    for line in lines:
        print(line)

        # Line setup
        line_index = lines.index(line)
        start_position_line = 0         # Start position of string on a line
        last_line_index = (len(lines) - 1)

        graphics = []

        while True:
            graphic_item = []

            if len(line) < 1:
                break

            # Start position
            top_line = lines[(line_index - 1)][start_position_line:]
            line = lines[(line_index)][start_position_line:]
            # TODO: imporve structure
            try:
                bottom_line = lines[(line_index + 1)][start_position_line:]
            except IndexError:
                bottom_line = None

            # Find next digit
            try:
                match_digit = re.search(r'\*', line)
                digit = match_digit.group()
                digit_start_index = match_digit.start()
                digit_end_index = match_digit.end()
            except AttributeError:
                break

            # Initialize setup left, right
            left = None
            right = None

            if digit_start_index == 0:
                left = digit_start_index
            else:
                left = digit_start_index - 1

            right = digit_end_index + 1

            # TOP SEARCH
            if line_index != 0:
                # If last index
                if digit_end_index == len(top_line) - 1:
                    right -= 1

                top_search = top_line[left:right]
                top_search = left_right_digit_search(left, right, top_search, top_line)

            # SIDE SEARCH
            if digit_end_index == len(line) - 1:
                right -= 1

            side_search = line[left:right]
            side_search = left_right_digit_search(left, right, side_search, line)

            # BOTTOM SEARCH
            if line_index < last_line_index:
                # If last index
                if digit_end_index == len(bottom_line) - 1:
                    right -= 1

                bottom_search = bottom_line[left:right]
                bottom_search = left_right_digit_search(left, right, bottom_search, bottom_line)

            # New start position
            start_position_line += digit_end_index

            # Store content to graphic element
            if top_search:
                graphic_item.append(top_search)

            if side_search:
                graphic_item.append(side_search)

            if bottom_search:
                graphic_item.append(bottom_search)

            # Check if valid digit
            items_to_multiply = []
            for gi in graphic_item:
                item_strings = re.split(r'[.*]', gi)

                for i in item_strings:
                    if str(i).isdigit():
                        items_to_multiply.append(int(i))

            # Calculate items
            if len(items_to_multiply) == 2:
                sum += (items_to_multiply[0]*items_to_multiply[1])

            graphics.append(graphic_item)
        # print()
        # print("Graphics")
        # print(graphics)
        # print()

result = sum

# Print result
print("\nAdvent of Code 2023")
print("Day 3\n")
print("\tResult:", result)
print()
