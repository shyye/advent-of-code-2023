from main import Main

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()


# Code
with open(FILE_PATH) as file:
    content = file.readlines()

result = None


# Print Result
main.set_result(result)
main.print_result_part2()
