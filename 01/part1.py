# RESOURCES:
# https://www.geeksforgeeks.org/extract-numbers-from-a-text-file-and-add-them-using-python/
# https://www.w3schools.com/python/python_howto_reverse_string.asp

# Constants
FILE_PATH = "01\\input.txt"

# Store sum
sum = 0

# Get file content - NOT GOOD PRACTICE, NEVER CLOSES FILE
# file = open(FILE_PATH, "r").readlines()
file_ = open(FILE_PATH, "r")
file = file_.readlines()

# Find first and last digit
for line in file:

    # Store digit
    digit = ""

    # Find first digit
    for char in line:
        if char.isdigit():
            digit += char
            break

    # Find last digit
    reversed_line = line[::-1]
    for char in reversed_line:
        if char.isdigit():
            digit += char
            break

    # Add to sum
    sum += int(digit)

file_.close()

# Print result
print("\nAdvent of Code 2023 - Day 1, Part 1\n")
print("Result:", sum)
print()
