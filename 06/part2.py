from main import Main

# File Setup
main = Main()
DAY = main.get_day()
FILE_PATH = main.get_filepath()


# Code
with open(FILE_PATH) as file:
    row_1 = file.readline().split(":")[1].strip()
    row_2 = file.readline().split(":")[1].strip()

time = int(row_1.replace(" ", ""))
record_distance = int(row_2.replace(" ", ""))

result = 1


num_of_ways_to_beat_record = 0

time_limit = time
record = record_distance

speed = 0

for time in range(1, time_limit-1):
    speed = time
    time_to_move = time_limit - time
    distance = time_to_move * speed

    if distance > record:
        num_of_ways_to_beat_record += 1

result = num_of_ways_to_beat_record


# Print Result
main.set_result(result)
main.print_result_part2()
